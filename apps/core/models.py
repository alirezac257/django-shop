from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        abstract = True


class BaseDiscount(BaseModel):
    DISCOUNT_TYPE_PERCENT = "percent"
    DISCOUNT_TYPE_NON_PERCENT = "non-percent"

    TYPE_CHOICES = (
        (_("تخفیف درصدی"),),
        (_("تخفیف عددی"),),
    )
    FLAG_CHOICES = ()
    flag = models.CharField(verbose_name=_("discount flag"), choices=FLAG_CHOICES, max_length=100, null=True,
                            blank=True)
    amount = models.PositiveIntegerField(verbose_name=_("discount amount"))
    type = models.CharField(verbose_name=TYPE_CHOICES, max_length=100)
    valid_from = models.DateTimeField(verbose_name=_("discount start date"), default=timezone.now)
    valid_to = models.DateTimeField(verbose_name=_("discount expire date"))
    is_active = models.BooleanField(verbose_name=_("is active"), default=True)

    class Meta:
        abstract = True
