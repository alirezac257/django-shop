from apps.core.models import BaseDiscount
from apps.product.models import Discount, Product
from apps.order.models import OrderDiscount


class DiscountService:
    def __init__(self):
        ...

    def calculate_product_discount_value(self, discount_uid: str):
        ...
