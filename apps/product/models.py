import uuid
from django.utils.translation import gettext_lazy as _
from django.db import models
from django_countries.fields import CountryField
from apps.core.models import BaseModel, BaseDiscount
from apps.order.models import Order


class Product(BaseModel):
    uid = models.CharField(verbose_name=_("uid"), default=uuid.UUID, unique=True, max_length=64)
    eng_name = models.CharField(verbose_name=_("english name"), max_length=100)
    fa_name = models.CharField(verbose_name=_("persian name"), max_length=100)
    price = models.PositiveIntegerField(verbose_name=_("price"))
    is_available = models.BooleanField(verbose_name=_("is available"), default=True)
    stock = models.PositiveIntegerField(verbose_name=_("stock"), default=0)
    brand = models.ForeignKey("Brand", verbose_name=_("brand"), on_delete=models.CASCADE, blank=True, null=True)
    discount = models.ForeignKey("Discount", verbose_name=_("discount"), on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.uid


# product Discount
class Discount(BaseDiscount):
    def __str__(self):
        return f"{self.amount} percent" if self.type == BaseDiscount.DISCOUNT_TYPE_PERCENT \
            else f"{self.amount}"


class Brand(BaseModel):
    name = models.CharField(verbose_name=_("name"), max_length=100)
    country = CountryField()
    logo = models.FileField(upload_to="brands/", default="default_images/brand.png")

    def __str__(self):
        return self.name
