from .models import Product, Discount
from apps.core.models import BaseDiscount
from django.utils import timezone
from .exceptions import BadRequest


class ProductService:
    def __init__(self):
        pass

    def calculate_discount_amount(self, product: Product):
        try:
            discount = Discount.objects.get(product=product, is_active=True, valid_from__lte=timezone.now, valid_to_gte=timezone.now)
            if discount.type == BaseDiscount.DISCOUNT_TYPE_NON_PERCENT:
                return discount.amount

            return product.price - ((product.price * discount.amount) * 100)
        except Discount.DoesNotExist:
            raise BadRequest("Discount does not exist or not valid")
