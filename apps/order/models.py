import uuid
from apps.core.models import BaseDiscount
from django.db import models
from apps.core.models import BaseModel
from django.utils.translation import gettext_lazy as _


class Order(BaseModel):
    uid = models.CharField(max_length=100, default=uuid.UUID)
    discount = models.ForeignKey("OrderDiscount", verbose_name=_("discount"), on_delete=models.CASCADE,
                                 related_name="orders")

    def __str__(self):
        return self.uid


class OrderDiscount(BaseDiscount):
    FLAG_CHOICES = ()  # TODO: create flag choices
    code = models.CharField(verbose_name=_("discount code"), max_length=20)

    def __str__(self):
        return f"{self.code} - {self.amount} percent" if self.type == BaseDiscount.DISCOUNT_TYPE_PERCENT \
            else f"{self.code} - {self.amount}"
